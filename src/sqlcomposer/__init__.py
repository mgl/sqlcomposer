# SPDX-License-Identifier: GPL-3.0-only
# Copyright (C) 2023 Michał Góral.

from .queries import QueryLoader, Another, AnotherGroup, Script
