run:
	poetry run flask run

black:
	poetry run black src test

check:
	poetry run pytest test --cov=sqlcomposer --cov-branch --no-cov-on-fail --cov-fail-under=90

pdb:
	poetry run pytest test -x --pdb

coverage:
	poetry run pytest --cov=sqlcomposer --cov-branch --cov-report=html:coverage-full --cov-fail-under=90

.PHONY: run black check coverage coverage-integration
